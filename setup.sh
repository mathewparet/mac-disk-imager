#!/bin/bash

usage()
{
	echo ""
	echo "$(basename $0) [-bihz] <filename>"
	echo "Where"
	echo "       -b    Backup to <filename>"
	echo "       -h    Show this help screen"
	echo "       -i    Install from <filename>"
	echo "       -z    Compress backedup file"
	echo ""
}

choose_drive()
{
	echo "Please choose external drive:"
	echo ""
	printf "No. %s\n" "$(df -h | head -1)"
	COUNT=0
	
	for drives in $(df -h | grep '^/' | grep -v 'disk1' | tr ' ' '|'); do
		COUNT=$(echo "$COUNT+1" |bc)
		printf "%3d %s\n" $COUNT "$(echo $drives | tr '|' ' ')"
	done
	echo ""
	echo ""
	while true
	do
		printf "Enter your choice (0 quits): "
		read CHOICE
		if [[ $CHOICE -eq 0 ]]
			then
			exit
		fi
		if [[ $CHOICE -le $COUNT && $CHOICE -gt 0 ]]
			then
			break
		else
			printf "Wrong choice. "
		fi
	done
	echo ""
	return $CHOICE
}

get_choice()
{
	CHOICE=$1
	echo $(df -h | grep '^/' | grep -v 'disk1' | head -$(echo $CHOICE) | tail -1 | awk '{print $1}')
}

backup()
{
	FILENAME=$1
	choose_drive
	DRIVE=$?
	DRIVE=$(get_choice $DRIVE)
	DRIVE=${DRIVE%${DRIVE:(-2)}}
	echo "You selected: " $DRIVE
	echo ""
	echo "Backup will initiate in 5 seconds. Press CTRL+C to cancel."
	echo ""
	sleep 5
	echo "Unmounting external disk..."
	diskutil unmountDisk $DRIVE
	echo "Starting backup..."
	pv -tpreb $DRIVE | dd bs=1m of=$FILENAME
	echo "Remounting external disk..."
	diskutil mountDisk $DRIVE
	if [[ $opt_compress -eq 1 ]]
		then
		echo "Compressing backup file..."
		gzip $FILENAME
	fi
	echo "Backup created."
}

install()
{
	FILENAME=$1
	choose_drive
	DRIVE=$?
	DRIVE=$(get_choice $DRIVE)
	DRIVE=${DRIVE%${DRIVE:(-2)}}
	echo "You selected: " $DRIVE
	echo ""
	echo "Installation will initiate in 5 seconds. You will loose all data/partitions in $DRIVE. Press CTRL+C to cancel."
	echo ""
	sleep 5
	echo "Unmounting external disk..."
	diskutil unmountDisk $DRIVE
	echo "Starting installation..."
	pv -tpreb $FILENAME | dd bs=1m of=$DRIVE
	echo "Remounting external disk..."
	diskutil mountDisk $DRIVE
	echo "Drive reinstalled"
}

echo ""
echo "Welcome to the Glibix Pi Setup Tool"
echo "==================================="
echo ""



while getopts "i:b:hz" flag
do
	case "$flag" in 
		b) backup "$OPTARG";;
	 	h) usage
			exit;;
		z) opt_compress=1;;
	 	i) install "$OPTARG";;
		:) echo "Error -$OPTARG requires an argument"
			usage
			exit;;
		?) echo "Error unknown option -$OPTARG"
			usage
			exit;;
		*) usage
			exit;;
	esac
done

